//data yang mau di uji
const d = [275, 40, 430];

// proses looping yang akan me return nilai yang di inginkan berdasarkan kondisi yang di penuhi
for (let i = 0; i < d.length; i++) { 
  const b = d[i];
  let t;
  if (b >= 50 && b <= 300) t = b * 0.15; else t = b * 0.2;
  const c = b + t;
  console.log(`Tagihan: ${b}, tip: ${t}, total: ${c}`);
}